package org.javalabs;

public class HelloWorldGreeting implements Greeting {

	@Override
	public void perform() {
		System.out.println("Hello world!");
	}

} // The End of Class
